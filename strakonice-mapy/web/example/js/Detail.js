class LayerDetail extends Panel {
    constructor(layerId) {
        super(elementIDs.layerDetailPanel)

        this.layer = map.queryRenderedFeatures().filter(e => e.id === layerId);
        this.etguid = this.layer[0].properties.etguid;
        this.build();
    }

    build() {
        this.titleElement = document.createElement('div');
        this.textElement = document.createElement('p');

        this.titleElement.textContent = "Test";
        this.textElement.textContent = "Test";

        this.titleElement.classList.add('et-panel-layer-detail-title');
        this.textElement.classList.add('et-panel-layer-detail-text');

        this.backButton();      //creates variable -> this.backButton
        this.closeButton();     //creates variable -> this.closeButton
        this.rating();          //creates variable -> this.ratingContainer
        this.commentSection();  //creates variable -> this.commentList 

        super.addChild(this.closeButton);
        super.addChild(this.backButton);
        super.addChild(this.titleElement);
        super.addChild(this.textElement);
        super.addChild(this.starsAndButtoncontainer);
        super.addChild(this.commentList);
    }

    closeButton() {
        this.closeButton = document.createElement('div');
        this.closeButtonIcon = new MaterialIcon('close', ['material-icons']);
        this.closeButton.classList.add('et-panel-close-button');

        this.closeButton.addEventListener('click', e => {
            super.hide();
            layersPanel.hide();
        })

        this.closeButton.appendChild(this.closeButtonIcon);

        //return this.closeButton;
    }

    backButton() {
        this.backButton = document.createElement('div');
        this.backButtonIcon = new MaterialIcon('keyboard_arrow_left', ['material-icons']);
        this.backButton.classList.add('et-panel-back-button');

        this.backButton.addEventListener('click', e => {
            //hideUnhideElement(this.destinationElement);
            //hideUnhideElement(elementIDs.layers);

            layersPanel.show();
            layerDetailPanel.hide();
        })

        this.backButton.appendChild(this.backButtonIcon);

        //return this.closeButton;
    }

    rating() {
        this.starsAndButtoncontainer = document.createElement('div');
        this.sendButtonContainer = document.createElement('div');
        this.ratingContainer = document.createElement('div');
        this.starsContainer = document.createElement('div');
        this.commentContainer = document.createElement('div');

        this.sendButton = document.createElement('button');
        this.commentInput = document.createElement('input');

        this.starsContainer = createStars(5);

        this.starsAndButtoncontainer.classList.add('et-panel-layer-detail-rating-container');
        this.ratingContainer.classList.add('et-panel-layer-detail-rating-input')
        this.sendButtonContainer.classList.add('et-panel-layer-detail-send-button');
        this.starsContainer.classList.add('et-layer-detail-rating-stars');
        this.commentContainer.classList.add('et-layer-detail-comment-section');
        this.commentInput.placeholder = "Vlozte komentar";

        this.sendButton.textContent = "Odeslat hodnoceni";
        this.sendButton.addEventListener('click', () => {
            this.starsValue = Array.from(this.starsContainer.querySelectorAll('input[type="radio"]')).filter(e => e.checked)[0];
            if (this.starsValue === undefined) this.starsValue = 0
            else this.starsValue = this.starsValue.value;
            sendReview(this.layer[0].properties.etguid, this.starsValue, this.commentInput.value);
        }, false)

        this.sendButtonContainer.appendChild(this.sendButton);
        this.commentContainer.appendChild(this.commentInput);
        this.ratingContainer.appendChild(this.starsContainer);
        this.ratingContainer.appendChild(this.sendButtonContainer);

        this.starsAndButtoncontainer.appendChild(this.ratingContainer);
        this.starsAndButtoncontainer.appendChild(this.commentContainer);
        //return this.ratingContainer;
    }

    commentSection(comments) {
        if (this.commentList === undefined) {
            this.commentList = document.createElement('ul');
            this.commentList.classList.add('et-panel-layer-detail-rating-list');
        }

        // if (comments) {
        //     comments.filter(e => e.etguid === '312f')[0].comments.forEach(e => {
        //         this.listItem = new CommentItem(e).itemElement;
        //         this.commentList.appendChild(this.listItem);
        //     })
        // }
        if (comments) {
            this.etguid = '123d';
            comments.filter(e => e.etguid === this.etguid).forEach(e => {
                this.listItem = new CommentItem(e).itemElement;
                this.commentList.prepend(this.listItem);
            })
        }
    }
}

class CommentItem {
    constructor(commentObject) {
        this.user = commentObject.user;
        this.numberOfStars = commentObject.rating;
        this.content = commentObject.content;

        this.build();
    }

    build() {
        this.itemElement = document.createElement('li');
        this.headerElement = document.createElement('div');
        this.headerName = document.createElement('span');
        this.headerStarsContainer = document.createElement('div');
        this.itemTextElement = document.createElement('p');

        this.headerName.textContent = this.user;
        this.itemTextElement.textContent = this.content;

        this.itemElement.classList.add('et-layer-detail-rating-list-item');
        this.headerElement.classList.add('et-rating-list-item-header');
        this.headerName.classList.add('et-rating-list-item-name');
        this.headerStarsContainer.classList.add('et-rating-list-item-rating');
        this.itemTextElement.classList.add('et-rating-list-item-comment');

        this.icons();

        this.headerElement.appendChild(this.headerName);
        this.headerElement.appendChild(this.headerStarsContainer);
        this.itemElement.appendChild(this.headerElement);
        this.itemElement.appendChild(this.itemTextElement);
    }

    icons() {

        for (var i = 1; i <= this.numberOfStars && i <= 5; i++) {
            
            this.headerStarsContainer.appendChild(new MaterialIcon("star", ['material-icons', 'rating-yellow']));
        }
        for (var i = 1; i <= (5 - this.numberOfStars) && i > 0; i++) {
            this.headerStarsContainer.appendChild(new MaterialIcon("star_border", ['material-icons', 'rating-yellow']));
        }
    }
}

class MaterialIcon {
    constructor(textContent, classes) {
        this.iconName = textContent;
        this.classes = classes;

        this.build();

        return this.iconElement;
    }

    build() {
        this.iconElement = document.createElement('i');

        this.classes.forEach(className => {
            this.iconElement.classList.add(className);
        });

        this.iconElement.textContent = this.iconName;

        //return this.iconElement;
    }
}

function createStars(numOfStars) {
    var star;
    let container = document.createElement('div');

    for (var i = 1; i <= numOfStars; i++) {
        star = document.createElement('input');
        starLabel = document.createElement('label');

        star.name = 'rating';
        star.type = 'radio';
        star.value = i;
        star.id = `star${i}`;

        starLabel.classList.add('full');
        starLabel.setAttribute('for', star.id);

        container.prepend(starLabel);
        container.prepend(star);
    }

    return container;
}