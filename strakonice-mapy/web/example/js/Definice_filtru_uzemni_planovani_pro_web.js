{
    "filters" [
        {
            "filter-type": 8,
            "options": [
                { "option-type": 9, "option-title": "2017", "option-value": "mapbox://styles/eternal1/cjy6zoh4u0wc81cn07pguqjau"},
                { "option-type": 9, "option-title": "2018", "option-value": "mapbox://styles/eternal1/cjy8jc4km0yfj1cpgd8nuofo2"},
                { "option-type": 9, "option-title": "2019", "option-value": "mapbox://styles/eternal1/cjxzz9yq10u401cpeg7ehi7op"},
            ]
        },
        {
        	"filter-type": 6,
            "options": [
                { "option-type": 2, "option-title": "Plochy s rozdílným způsobem využití" ,"option-value": "plocha_bydleni,plocha_bydleni_stav,bydleni_hromadne_v_bytovych_domech_blokova_zastavba,bydleni_hromadne_v_bytovych_domech_blokova_zastavba_stav,bydleni_individualni_v_rodinnych_domech_mestske,bydleni_individualni_v_rodinnych_domech_mestske_stav,bydleni_individualni_v_rodinnych_domech_vesnicke,bydleni_individualni_v_rodinnych_domech_vesnicke_stav,plocha_rekreace,blocha_rekreace_stav,plocha_rodinne_rekreace,plocha_rodinne_rekreace_stav,plocha_individualni_rekreace_zahradkarska_osada,plocha_individualni_rekreace_zahradkarska_osada_stav,plocha_obcanskeho_vybaveni,plocha_obcanskeho_vybaveni_stav,plocha_verejne_vybavenosti,plocha_verejne_vybavenosti_stav,plocha_pro_vzdelavani_a_vychovu,plocha_pro_vzdelavani_a_vychovu_stav,plocha_komercnich_zarizeni,plocha_komercnich_zarizeni_stav,plocha_pro_telovychovu_a_sport,plocha_pro_telovychovu_a_sport_stav,plocha_smisena_obytna,plocha_smisena_obytna_stav,plocha_smisena_obytna_komercni,plocha_smisena_obytna_komercni_stav,plocha_dopravni_infrastruktury,plocha_dopravni_infrastruktury_stav,plocha_pro_silnicni_dopravu,plocha_pro_silnicni_dopravu_stav,plocha_pro_drazni_dopravu,plocha_pro_drazni_dopravu_stav,plocha_pro_leteckou_dopravu,plocha_pro_leteckou_dopravu_stav,plocha_pro_vodni_hospodarstvi,plocha_pro_vodni_hospodarstvi_stav,plocha_vyroby_a_skladovani,plocha_vyroby_a_skladovani_stav,prumyslova_vyroba_a_sklady_lehky_prumysl,prumyslova_vyroba_a_sklady_lehky_prumysl_stav,plocha_pro_zemedelskou_a_lesnickou_vyrobu,plocha_pro_zemedelskou_a_lesnickou_vyrobu_stav,plocha_verejneho_prostranstvi_s_prevahou_zpevnenych_ploch,plocha_verejneho_prostranstvi_s_prevahou_zpevnenych_ploch_stav,plocha_verejnych_prostranstvi_s_prevahou_nezpevnenych_ploch,plocha_verejnych_prostranstvi_s_prevahou_nezpevnenych_ploch_stav,park_historicka_zahrada,park_historicka_zahrada_stav"},	
 				{ "option-type": 3, "option-title": "Plocha bydlení", "option-class-id": "plocha_bydleni", "option-value": "plocha_bydleni,plocha_bydleni_stav"},
 				{ "option-type": 3, "option-title": "Bydlení hromadné v bytových domech - bloková zástavba", "option-class-id": "bydleni_hromadne_v_bytovych_domech_blokova_zastavba", "option-value": "bydleni_hromadne_v_bytovych_domech_blokova_zastavba,bydleni_hromadne_v_bytovych_domech_blokova_zastavba_stav"},
 				{ "option-type": 3, "option-title": "Bydlení individuální v rodinných domech - městské", "option-class-id": "bydleni_individualni_v_rodinnych_domech_mestske", "option-value": "bydleni_individualni_v_rodinnych_domech_mestske,bydleni_individualni_v_rodinnych_domech_mestske_stav"},
 				{ "option-type": 3, "option-title": "Bydlení individuální v rodinných domech - vesnické", "option-class-id": "bydleni_individualni_v_rodinnych_domech_vesnicke", "option-value": "bydleni_individualni_v_rodinnych_domech_vesnicke,bydleni_individualni_v_rodinnych_domech_vesnicke_stav"},
 				{ "option-type": 3, "option-title": "Plocha rekreace", "option-class-id": "plocha_rekreace", "option-value": "plocha_rekreace,plocha_rekreace_stav"},
 				{ "option-type": 3, "option-title": "Plocha rodinné rekreace", "option-class-id": "plocha_rodinne_rekreace", "option-value": "plocha_rodinne_rekreace,plocha_rodinne_rekreace_stav"},
				{ "option-type": 3, "option-title": "Plocha individuální rekreace - zahrádkářská osada", "option-class-id": "plocha_individualni_rekreace_zahradkarska_osada", "option-value": "plocha_individualni_rekreace_zahradkarska_osada,plocha_individualni_rekreace_zahradkarska_osada_stav"},		
				{ "option-type": 3, "option-title": "Plocha občanského vybavení", "option-class-id": "plocha_obcanskeho_vybaveni", "option-value": "plocha_obcanskeho_vybaveni,plocha_obcanskeho_vybaveni_stav"},
				{ "option-type": 3, "option-title": "Plocha veřejné vybavenosti", "option-class-id": "plocha_verejne_vybavenosti", "option-value": "plocha_verejne_vybavenosti,plocha_verejne_vybavenosti_stav"},		
				{ "option-type": 3, "option-title": "Plocha pro vzdělávání a výchovu", "option-class-id": "plocha_pro_vzdelavani_a_vychovu", "option-value": "plocha_pro_vzdelavani_a_vychovu,plocha_pro_vzdelavani_a_vychovu_stav"},
				{ "option-type": 3, "option-title": "Plocha komerčních zařízení", "option-class-id": "plocha_komercnich_zarizeni", "option-value": "plocha_komercnich_zarizeni,plocha_komercnich_zarizeni_stav"},
				{ "option-type": 3, "option-title": "Plocha pro tělovýchovu a sport", "option-class-id": "plocha_pro_telovychovu_a_sport", "option-value": "plocha_pro_telovychovu_a_sport,plocha_pro_telovychovu_a_sport_stav"},
				{ "option-type": 3, "option-title": "Plocha smíšená obytná", "option-class-id": "plocha_smisena_obytna", "option-value": "plocha_smisena_obytna,plocha_smisena_obytna_stav"},
				{ "option-type": 3, "option-title": "Plocha smíšená obytná - komerční", "option-class-id": "plocha_smisena_obytna_komercni", "option-value": "plocha_smisena_obytna_komercni,plocha_smisena_obytna_komercni_stav"},
				{ "option-type": 3, "option-title": "Plocha dopravní infrastruktury", "option-class-id": "plocha_dopravni_infrastruktury", "option-value": "plocha_dopravni_infrastruktury,plocha_dopravni_infrastruktury_stav"},
				{ "option-type": 3, "option-title": "Plocha pro silniční dopravu", "option-class-id": "plocha_pro_silnicni_dopravu", "option-value": "plocha_pro_silnicni_dopravu,plocha_pro_silnicni_dopravu_stav"},
				{ "option-type": 3, "option-title": "Plocha pro drážní dopravu", "option-class-id": "plocha_pro_drazni_dopravu", "option-value": "plocha_pro_drazni_dopravu,plocha_pro_drazni_dopravu_stav"},
				{ "option-type": 3, "option-title": "Plocha pro leteckou dopravu", "option-class-id": "plocha_pro_leteckou_dopravu", "option-value": "plocha_pro_leteckou_dopravu,plocha_pro_leteckou_dopravu_stav"},
				{ "option-type": 3, "option-title": "Plocha pro vodní hospodářství", "option-class-id": "plocha_pro_vodni_hospodarstvi", "option-value": "plocha_pro_vodni_hospodarstvi,plocha_pro_vodni_hospodarstvi_stav"},
				{ "option-type": 3, "option-title": "Plocha výroby a skladování", "option-class-id": "plocha_vyroby_a_skladovani", "option-value": "plocha_vyroby_a_skladovani,plocha_vyroby_a_skladovani_stav"},
				{ "option-type": 3, "option-title": "Průmyslová výroba a sklady - lehký průmysl", "option-class-id": "prumyslova_vyroba_a_sklady_lehky_prumysl", "option-value": "prumyslova_vyroba_a_sklady_lehky_prumysl,prumyslova_vyroba_a_sklady_lehky_prumysl_stav"},
				{ "option-type": 3, "option-title": "Plocha pro zemědělskou a lesnickou výrobu", "option-class-id": "plocha_pro_zemedelskou_a_lesnickou_vyrobu", "option-value": "plocha_pro_zemedelskou_a_lesnickou_vyrobu,plocha_pro_zemedelskou_a_lesnickou_vyrobu_stav"},
				{ "option-type": 3, "option-title": "Plocha veřejného prostranství s převahou zpevněných ploch", "option-class-id": "plocha_verejneho_prostranstvi_s_prevahou_zpevnenych_ploch", "option-value": "plocha_verejneho_prostranstvi_s_prevahou_zpevnenych_ploch,plocha_verejneho_prostranstvi_s_prevahou_zpevnenych_ploch_stav"},
				{ "option-type": 3, "option-title": "Plocha veřejného prostranství s převahou nezpevněných ploch", "option-class-id": "plocha_verejnych_prostranstvi_s_prevahou_nezpevnenych_ploch", "option-value": "plocha_verejnych_prostranstvi_s_prevahou_nezpevnenych_ploch, plocha_verejnych_prostranstvi_s_prevahou_nezpevnenych_ploch_stav"},
				{ "option-type": 3, "option-title": "Park - historická zahrada", "option-class-id": "park_historicka_zahrada", "option-value": "park_historicka_zahrada, park_historicka_zahrada_stav"},		
				
				{ "option-type": 2, "option-title": "Základní členění území", "option-value": "hranice_obce,hranice_zastaveneho_uzemi_obce,zastavitelna_plocha,plocha_pro_zpracovani_regulacniho_planu,plocha_pro_overeni_uzemni_studii"},
				{ "option-type": 3, "option-title": "Hranice obce", "option-class-id": "hranice_obce", "option-value": "hranice_obce"},
				{ "option-type": 3, "option-title": "Hranice zastavěného území", "option-class-id": "hranice_zastaveneho_uzemi_obce", "option-value": "hranice_zastaveneho_uzemi_obce"},
				{ "option-type": 3, "option-title": "Zastavitelná plocha", "option-class-id": "zastavitelna_plocha", "option-value": "zastavitelna_plocha"},
				{ "option-type": 3, "option-title": "Plocha pro zpracování regulačního plánu", "option-class-id": "plocha_pro_zpracovani_regulacniho_planu", "option-value": "plocha_pro_zpracovani_regulacniho_planu"},
				{ "option-type": 3, "option-title": "Plocha pro ověření územních studií", "option-class-id": "plocha_pro_overeni_uzemni_studii", "option-value": "plocha_pro_overeni_uzemni_studii"},
				
				{ "option-type": 2, "option-title": "Koncepce technické infrastruktury", "option-value": "op_teplarenske_site,vedeni_elektricke_site_zvn,vedeni_elektricke_site_vvn,vedeni_elektricke_site_podzemni_vn_stav_1,vedeni_elektricke_site_nadzemni_vn_stav_1,vedeni_elektricke_site_podzemni_vn_stav_2,vedeni_elektricke_site_nadzemni_vn_stav_2,op_vedeni_elektricke_site_naphlad_0,op_vedeni_elektricke_site_naphlad_15,op_vedeni_elektricke_site,plynovod_vtl_stav_1,plynovod_vtl_stav_2,plynovod_stl_stav_1,plynovod_stl_stav_2,op_vodni_rad_stav_1,op_vodni_rad_stav_2"},
				{ "option-type": 3, "option-title": "Vedení elektrické sítě ZVN", "option-class-id": "vedeni_elektricke_site_zvn", "option-value": "vedeni_elektricke_site_zvn"},
				{ "option-type": 3, "option-title": "Vedení elektrické sítě VVN", "option-class-id": "vedeni_elektricke_site_vvn", "option-value": "vedeni_elektricke_site_vvn"},
				{ "option-type": 3, "option-title": "Vedení elektrické sítě VN podzemní", "option-class-id": "vedeni_elektricke_site_podzemni_vn", "option-value": "vedeni_elektricke_site_podzemni_vn_stav_1,vedeni_elektricke_site_podzemni_vn_stav_2"},
				{ "option-type": 3, "option-title": "Vedení elektrické sítě VN nadzemní", "option-class-id": "vedeni_elektricke_site_nadzemni_vn", "option-value": "vedeni_elektricke_site_nadzemni_vn_stav_1,vedeni_elektricke_site_nadzemni_vn_stav_2"},
				{ "option-type": 3, "option-title": "OP vedení elektrické sítě", "option-class-id": "op_vedeni_elektricke_site", "option-value": "op_vedeni_elektricke_site_naphlad_0,op_vedeni_elektricke_site_naphlad_15,op_vedeni_elektricke_site"},
				{ "option-type": 3, "option-title": "Plynovod VTL", "option-class-id": "plynovod_vtl", "option-value": "plynovod_vtl_stav_1,plynovod_vtl_stav_2"},
				{ "option-type": 3, "option-title": "Plynovod STL", "option-class-id": "plynovod_stl", "option-value": "plynovod_stl_stav_1,plynovod_stl_stav_2"},
				{ "option-type": 3, "option-title": "OP teplárenské sítě", "option-class-id": "op_teplarenske_site", "option-value": "op_teplarenske_site"},
				{ "option-type": 3, "option-title": "OP vodní řád", "option-class-id": "op_vodni_rad", "option-value": "op_vodni_rad_stav_1,op_vodni_rad_stav_2"},
				
				{ "option-type": 2, "option-title": "Limity využití území", "option-value": "plocha_pri_okraji_lesa_s_podminenym_vyuzivanim,zaplavove_uzemi_s_periodicitou_100_let"},
				{ "option-type": 3, "option-title": "Plocha při okraji lesa s podmíněným využíváním", "option-class-id": "plocha_pri_okraji_lesa_s_podminenym_vyuzivanim", "option-value": "plocha_pri_okraji_lesa_s_podminenym_vyuzivanim"},
				{ "option-type": 3, "option-title": "Záplavové území s periodicitou 100 let", "option-class-id": "zaplavove_uzemi_s_periodicitou_100_let", "option-value": "zaplavove_uzemi_s_periodicitou_100_let"}
            ]
		}
    ]
}


