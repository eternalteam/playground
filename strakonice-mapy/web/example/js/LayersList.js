class LayersList extends Panel {
    constructor(features) {
        super(elementIDs.layersPanel);

        this.features = features;
        //this.destinationElement = document.getElementById(destinationElementId);)
    }

    build() {
        super.clearPanel();

        this.closeButton = document.createElement('div');
        this.closeButtonIcon = document.createElement('i');
        this.ul = document.createElement('ul');

        this.closeButton.classList.add('et-panel-close-button');
        this.closeButtonIcon.classList.add('material-icons');
        this.ul.classList.add('et-panel-layer-list');

        this.closeButtonIcon.textContent = 'close';
        this.closeButton.addEventListener('click', e => {
            super.hide();
        });


        this.features.forEach((element, index) => {
            this.listItem = new LayersListItem(element).build();
            this.ul.appendChild(this.listItem);
        });

        this.closeButton.appendChild(this.closeButtonIcon);

        super.addChild(this.closeButton);
        super.addChild(this.ul);
        super.show();
    }
}

class LayersListItem {
    constructor(item) {
        this.selectorId = item.id;
        this.properties = item.properties;
        // this.title = this.properties["NAZEV_JEVU"];
        // this.village = this.properties["OBEC"];
        this.etText = this.properties["ettext"];
        this.etTitle = this.properties["ettitle"];
        this.id = this.properties.id;
    }

    build() {
        this.li = document.createElement('li');
        this.headerDiv = document.createElement('div');
        this.titleDiv = document.createElement('div');
        this.descriptionDiv = document.createElement('div');
        this.ratingDiv = document.createElement('div');

        for (var i = 0; i < 6; i++) {
            this.star = document.createElement('i');
            this.star.classList.add('material-icons');
            this.star.classList.add('rating-yellow');

            switch (i) {
                case 3:
                    this.star.textContent = "star_half";
                    break;

                case 4:
                    this.star.textContent = "star_border";
                    break;

                case 5:
                    this.star.textContent = "star_border";
                    break;

                default:
                    this.star.textContent = "star";
                    break;
            }
            this.ratingDiv.appendChild(this.star);
        }

        this.li.classList.add('et-panel-layer-list-item');
        this.headerDiv.classList.add('et-panel-layer-list-item-header');
        this.titleDiv.classList.add('et-panel-layer-title');
        this.descriptionDiv.classList.add('et-panel-layer-desc');
        this.ratingDiv.classList.add('et-panel-layer-rating');

        this.titleDiv.textContent = this.etTitle;
        this.descriptionDiv.textContent = this.etText;

        this.headerDiv.appendChild(this.titleDiv);
        this.headerDiv.appendChild(this.ratingDiv);
        this.li.appendChild(this.headerDiv);
        this.li.appendChild(this.descriptionDiv);

        this.li.addEventListener('click', () => {
            layerDetail(this.selectorId)
        })

        return this.li
    }
}