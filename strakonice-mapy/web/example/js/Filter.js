/*-----------------------------------------------------------------------------*/
/* LAYERS FILTER PANEL */
class FilterList extends Panel {
    constructor(jsonCode) {
        super(elementIDs.layerFilterPanel);

        this.options = jsonCode.options;
        this.containerId = elementIDs.layerFilterPanel;


        this.findListItemsToHeaderItem()
    }
    
    buildList() {
        this.ul = document.createElement('ul');
        this.ul.classList.add('et-panel-filter-list');

        this.addItems();

        super.addChild(this.ul);
    }

    addItems() {
        for (var i in this.list) {
            this.item = new FilterListItem(this.list[i]).build();
            this.ul.appendChild(this.item)
        }
    }

    findListItemsToHeaderItem() {
        this.headerItemArray = [];
        this.listItemArray = [];
        this.list = [];

        for (var j in this.options) {
            this.itemProperites = this.options[j];

            if (this.itemProperites['option-type'] == 2) this.headerItemArray.push(this.itemProperites)
            if (this.itemProperites['option-type'] == 3) this.listItemArray.push(this.itemProperites)
        }

        this.headerItem = this.itemProperites;
        this.hederItemValues = this.headerItem['option-values'];

        this.headerItemArray.forEach(e => {
            this.filteredValues = this.listItemArray.filter(f => e['option-value'].indexOf(f['option-class-id']) >= 0)
            this.filteredValues.unshift(e);
            this.list = this.filteredValues;

            this.buildList();
        })
    }

    //super.show();
}

class FilterListItem {
    constructor(item) {
        this.options = item;
        this.type = this.options["option-type"];
        this.title = this.options["option-title"];
        this.colorBoxId = this.options["option-class-id"];
        this.color = this.options["option-color"];
        this.layers = this.options["option-value"];
        //this.createSpecificItem()
    }

    build() {
        this.li = document.createElement('li');
        this.span = document.createElement('span');
        this.input = document.createElement('input');

        //this.li.setAttribute('layers', this.value);
        this.li.classList.add('et-panel-filter-list-item');
        this.input.type = 'checkbox';
        this.input.checked = true;
        //this.input.value = this.layers;
        this.span.textContent = this.title;

        if (this.type === 3) {
            this.colorBox = document.createElement('div');
            this.colorBox.classList.add('et-filter-color-box');
            this.colorBox.style.background = this.color;
            this.colorBox.id = this.colorBoxId;
            this.li.appendChild(this.colorBox);

        } else if (this.type === 2) this.li.classList.add('header');

        [this.li, this.input].forEach(element => {
            element.addEventListener('click', () => {
                if (this.input.checked == true) {
                    this.input.checked = false;
                    this.input.parentElement.parentElement.firstElementChild.querySelector('input').checked = false;
                }
                else {
                    this.input.checked = true;
                    this.allListItemCheckboxes = this.input.parentElement.parentElement.querySelectorAll('li:not(:first-child) input[type="checkbox"]');

                    var allTrue = true;

                    Array.from(this.allListItemCheckboxes).forEach(e => {
                        if (e.checked === false) { allTrue = false }
                    })

                    if (allTrue) this.input.parentElement.parentElement.firstElementChild.querySelector('input').checked = true;
                }

                if (this.type === 2) {
                    this.checkboxes = this.li.parentElement.querySelectorAll('input[type="checkbox"]');
                    this.checkboxes.forEach(e => e.checked = this.input.checked);
                }

                hideUnhideLayers(this.layers, this.input.checked);
            })
        });

        this.li.appendChild(this.span);
        this.li.appendChild(this.input);
        return this.li;
    }
}
/*-----------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------*/
/* DATE FILTER PANEL */

class DateFilterList extends Panel {
    constructor(jsonCode) {
        super(elementIDs.dateFilterPanel);

        this.options = jsonCode.options;

        //this.clearDestinationElement()
        this.buildList();
    }

    clearDestinationElement() {
        while (this.destinationElement.firstChild) {
            this.destinationElement.removeChild(this.destinationElement.firstChild);
        }
    }

    buildList() {
        this.ul = document.createElement('ul');
        this.ul.classList.add('et-panel-filter-list');

        this.addItems();

        super.addChild(this.ul);
    }

    addItems() {
        for (var i in this.options) {
            this.item = new DateFilterListItem(this.options[i]).build();
            this.ul.appendChild(this.item)
        }
    }
}

class DateFilterListItem {
    constructor(item) {
        this.options = item;
        this.type = this.options["option-type"];
        this.title = this.options["option-title"];
        this.value = this.options["option-value"];
        this.valueID = this.value.split('/')[4];
        this.sprite = map.getStyle().sprite.split('/')[4];
    }

    build() {
        this.li = document.createElement('li');
        this.span = document.createElement('span');
        this.input = document.createElement('input');

        this.li.classList.add('et-panel-filter-list-item');
        this.span.textContent = this.title;
        
        this.input.type = 'radio';
        this.input.name = 'date'

        this.li.addEventListener('click', () => {
            this.input.checked = true

            map.setStyle(this.value);
        });

        if (this.valueID === this.sprite) this.input.checked = true;

        this.li.appendChild(this.span);
        this.li.appendChild(this.input);
        return this.li;
    }
}
/*-----------------------------------------------------------------------------*/