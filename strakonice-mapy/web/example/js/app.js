const elementIDs = {
	layerFilterPanel: "maps-filters-layer",
	dateFilterPanel: "maps-filters-date",
	layersPanel: "maps-layers",
	layerDetailPanel: "maps-layer-detail",
	layerFilterButton: "et-maps-layer-filter-button",
	dateFilterButton: "et-maps-date-filter-button",
	map: "map"
}


const filterTypes = {
	dateFilter: 8,
	layerFilter: 6,

};

var map = null;
let layerFilterPanel, dateFilterPanel, layersPanel, layerDetailPanel;

function initializeMap() {
	let mapElement = document.getElementById(elementIDs.map);
	let accessToken = mapElement.getAttribute('data-access-token');
	let style = mapElement.getAttribute('data-style');
	let defaultCoord = [49.261643438819675, 13.902854919433596];
	let defaultZoom = 14;
	var marker;


	//L.mapbox.accessToken = accessToken;



	// var leafletMap = L.map(mapElement)
	// .setView(defaultCoord, defaultZoom)
	// .styleLayer(style);

	// glMap = L.mapboxGL({
	//     style: style,
	//     accessToken: accessToken
	// }).addTo(leafletMap);

	// console.log(glMap);



	//map = glMap._glMap;



	// var leafletMap = L.map(mapElement);
	// console.log(leafletMap)
	// var gl = L.mapboxGL({
	//     accessToken: accessToken,
	//     style: style,
	// }).addTo(leafletMap);

	// leafletMap.on('click', function (e) {
	//     if (marker) marker.remove();
	//     marker = L.marker(e.latlng).addTo(leafletMap);
	// });
}


function initializeMap() {
	let mapElement = document.getElementById(elementIDs.map);

	if (mapElement) {
		let accessToken = mapElement.getAttribute('data-access-token');
		let style = mapElement.getAttribute('data-style');

		mapboxgl.accessToken = accessToken;
		map = new mapboxgl.Map({
			container: elementIDs.map,
			style: style,
		});

		map.on('click', function (e) {

			let popup = new mapboxgl.Popup({
				anchor: 'bottom',
				offset: [0, 22],
				closeButton: false
			});

			let content = document.createElement('i');
			content.classList.add('material-icons');
			content.textContent = "location_on";



			popup.setLngLat(e.lngLat)
				.addTo(map)
				.setDOMContent(content);

			content.parentNode.parentNode.className += ' pin1';

			let features = map.queryRenderedFeatures(e.point);

			getLayersByProperty(features, "etguid");
		});
	};
}







window.onload = () => {
	listenToFilterButton();
	listenToHidePanelButton();
	initializeMap();
}

function listenToFilterButton() {
	let buttons = document.querySelectorAll(`a#${elementIDs.layerFilterButton}, a#${elementIDs.dateFilterButton}`);

	buttons.forEach(e => e.addEventListener('click', () => {
		var filterType;

		if (e.id === elementIDs.layerFilterButton) filterType = filterTypes.layerFilter
		else if (e.id === elementIDs.dateFilterButton) filterType = filterTypes.dateFilter;

		if (Array.from(e.classList).indexOf('activeFilter') == -1) {
			let prevActiveFilterElement = Array.from(document.querySelectorAll('.activeFilter')).forEach(f => f.classList.remove("activeFilter"));
			//showElement(elementIDs.dateFilterPanel)
			e.classList.add("activeFilter");
		}
		else {
			//hideElement(elementIDs.dateFilterPanel);
			e.classList.remove('activeFilter');
		}


		createFiltersPanel(filterType, e.classList);

	}))
}



function listenToHidePanelButton() {
	let buttons = document.querySelectorAll(".et-panel-close-button");

	for (var i = 0; i < buttons.length; i++) {
		buttons[i].addEventListener('click', e => hideUnhideElement(e.currentTarget.parentElement.id))
	}
	//buttons.forEach(e => e.addEventListener('click', hideUnhideElement(e.parentElement.id)))
}

//gets ELEMENT or ELEMENT ID
//then checks if element is already hidden
//if yes, function unhides element
//in no, function hides element
function hideUnhideElement(id) {
	console.log(id)
	if (typeof id === "string") var target = document.getElementById(id);
	else target = id;

	let isHidden = Array.from(target.classList).indexOf('hidden') != -1 ? true : false;

	if (isHidden) showElement(id);
	else hideElement(id);
}

function hideElement(id) {
	if (typeof id === "string") var target = document.getElementById(id);
	else target = id;

	target.classList.add('hidden');
	if (id === elementIDs.layerDetail) document.getElementById(elementIDs.layersPanel).classList.remove('hidden');
}

function showElement(id) {
	if (typeof id === "string") var target = document.getElementById(id);
	else target = id;

	target.classList.remove('hidden');
}


//sorts out all given features by given property
function getLayersByProperty(features, property) {
	let filteredFeatures = features.filter(e => e.properties.etguid != null);
	//filteredFeatures.forEach(element => { new DetailList(element, layersElementId).build()});
	createLayersPanel(filteredFeatures)
}

function createLayersPanel(filteredFeatures) {
	//let layersElementId = elementIDs.layersPanel;
	//layersPanel = new LayersList(filteredFeatures);

	layersPanel = new LayersList(filteredFeatures)

	if (filteredFeatures.length > 0) layersPanel.build();
}

//function gets JSON data from tag with #map
//creates an object with filters
//puts it into tag with #filters
function createFiltersPanel(_filterType, classList) {
	let data = document.getElementById('map').getAttribute('data-filters');
	let filters = JSON.parse(data).filters;
	let filterType = _filterType * 1;
	let destination = document.getElementById(elementIDs.filters);
	let filteredValues = filters.filter(e => e['filter-type'] == filterType)[0];

	if (filterType === 6) {
		if (layerFilterPanel == null) layerFilterPanel = new FilterList(filteredValues);

		if (layerFilterPanel.visible == false) {
			layerFilterPanel.show();
			if (dateFilterPanel != null) dateFilterPanel.hide();
		}
		else if (layerFilterPanel.visible == true) {
			layerFilterPanel.hide();
		}

		countActiveFilters();
	} else if (filterType === 8) {
		if (dateFilterPanel == null) dateFilterPanel = new DateFilterList(filteredValues);

		if (dateFilterPanel.visible == false) {
			dateFilterPanel.show();
			if (layerFilterPanel != null) layerFilterPanel.hide();
		}
		else if (dateFilterPanel.visible == true) {
			dateFilterPanel.hide();
		}
	}
	//if (Array.from(classList).indexOf('activeFilter') != -1 || document.querySelectorAll('.activeFilter').length == 0) hideUnhideElement(destination);
}


function hideUnhideLayers(layers, visible) {
	let mapLayers = map.getStyle().layers;
	let filteredLayers = mapLayers.filter(e => layers.indexOf(e.id) >= 0)

	filteredLayers.forEach(layer => {

		if (visible === false) {
			map.setLayoutProperty(layer.id, 'visibility', 'none');
		} else {
			map.setLayoutProperty(layer.id, 'visibility', 'visible');
		}
	})

	countActiveFilters()
}

function countActiveFilters() {
	let allCheckboxes = Array.from(document.getElementById(elementIDs.layerFilterPanel).querySelectorAll('li:not(.header) input[type="checkbox"]'));
	let numberOfActiveFilters = allCheckboxes.filter(e => e.checked).length;

	document.getElementById('et-maps-layer-filter-button').firstElementChild.textContent = numberOfActiveFilters;
}

function layerDetail(layerId) {
	layerDetailPanel = new LayerDetail(layerId, elementIDs.layerDetail);

	layersPanel.hide();
	layerDetailPanel.show();

	displayComments();
}


function sendReview(etguid, stars, comment) {

	layerDetailPanel.commentSection([{ user: "user", etguid: '123d', rating: stars, content: comment }]);
	// const csrf_token = document.querySelector('head meta[name="csrf-token"]').content;
	// let data = {
	//     authenticity_token: csrf_token,
	//     feelingTypeId: document.querySelector('input[name="feelingTypeId"]:checked').value,
	//     description: document.getElementById('et-feeling-description').value,
	//     latitude: feeling.point.lat,
	//     longitude: feeling.point.lng
	// };
	// $.post(feeling.apiAddFeelingUrl, data)
	//     .done(function (msg) {
	//         feeling.control = new FeelingWelcomeControl(true);
	//     })
	//     .fail(function (xhr, status, error) {
	//         feeling.control = new FeelingWelcomeControl(false);
	//     });
}

function displayComments() {
	/*let commentsArray = [
		{
			etguid: '312f',
			comments: [
				{
					user: "radibfdkljsbfjkfabldjksblfsbdljkadsblm",
					rating: 1,
					content: "neasi"
				},
				{
					user: "radim",
					rating: 2,
					content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam expedita alias illo suscipit nemo nostrum voluptatum ducimus, vero aliquam assumenda mollitia delectus, beatae fugit ipsa dignissimos tenetur, distinctio natus repellendus."
				},
				{
					user: "radibfdkljsbfjkfabldjksblfsbdljkadsblm",
					rating: 4,
					content: "neasi"
				},
				{
					user: "radim",
					rating: 3,
					content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam expedita alias illo suscipit nemo nostrum voluptatum ducimus, vero aliquam assumenda mollitia delectus, beatae fugit ipsa dignissimos tenetur, distinctio natus repellendus."
				},
				{
					user: "radim",
					rating: 5,
					content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam expedita alias illo suscipit nemo nostrum voluptatum ducimus, vero aliquam assumenda mollitia delectus, beatae fugit ipsa dignissimos tenetur, distinctio natus repellendus."
				}
			]
		},
		{
			etguid: '3112312312f',
			comments: [
				{
					user: "radibfdkljsbfjkfabldjksblfsbdljkadsblm",
					rating: 1,
					content: "neasi"
				},
				{
					user: "radim",
					rating: 2,
					content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam expedita alias illo suscipit nemo nostrum voluptatum ducimus, vero aliquam assumenda mollitia delectus, beatae fugit ipsa dignissimos tenetur, distinctio natus repellendus."
				},
				{
					user: "radibfdkljsbfjkfabldjksblfsbdljkadsblm",
					rating: 4,
					content: "neasi"
				},
				{
					user: "radim",
					rating: 3,
					content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam expedita alias illo suscipit nemo nostrum voluptatum ducimus, vero aliquam assumenda mollitia delectus, beatae fugit ipsa dignissimos tenetur, distinctio natus repellendus."
				},
				{
					user: "radim",
					rating: 5,
					content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam expedita alias illo suscipit nemo nostrum voluptatum ducimus, vero aliquam assumenda mollitia delectus, beatae fugit ipsa dignissimos tenetur, distinctio natus repellendus."
				}
			]
		},
	];
	*/

	// let comments = [
	//     ["radim", '123d', 1, "neasi"],
	//     ["radim", '123d', 2, "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam expedita alias illo suscipit nemo nostrum voluptatum ducimus, vero aliquam assumenda mollitia delectus, beatae fugit ipsa dignissimos tenetur, distinctio natus repellendus."],
	//     ["radim", '123d', 3, "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam expedita alias illo suscipit nemo nostrum voluptatum ducimus, vero aliquam assumenda mollitia delectus, beatae fugit ipsa dignissimos tenetur, distinctio natus repellendus."],
	//     ["radim", '123d', 3, "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam expedita alias illo suscipit nemo nostrum voluptatum ducimus, vero aliquam assumenda mollitia delectus, beatae fugit ipsa dignissimos tenetur, distinctio natus repellendus."],
	//     ["radim", '123d', 3, "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam expedita alias illo suscipit nemo nostrum voluptatum ducimus, vero aliquam assumenda mollitia delectus, beatae fugit ipsa dignissimos tenetur, distinctio natus repellendus."],
	//     ["radim", '123d', 4, "neasi"],
	//     ["radim", '123d', 5, "neasi"],
	// ];

	let comments = [
		{ user: "radim", etguid: '123d', rating: 1, content: "neasi" },
		{ user: "radim", etguid: '123d', rating: 2, content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam expedita alias illo suscipit nemo nostrum voluptatum ducimus, vero aliquam assumenda mollitia delectus, beatae fugit ipsa dignissimos tenetur, distinctio natus repellendus." },
		{ user: "radim", etguid: '123d', rating: 3, content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam expedita alias illo suscipit nemo nostrum voluptatum ducimus, vero aliquam assumenda mollitia delectus, beatae fugit ipsa dignissimos tenetur, distinctio natus repellendus." },
		{ user: "radim", etguid: '123d', rating: 5, content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam expedita alias illo suscipit nemo nostrum voluptatum ducimus, vero aliquam assumenda mollitia delectus, beatae fugit ipsa dignissimos tenetur, distinctio natus repellendus." },
		{ user: "radim", etguid: '123d', rating: 3, content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam expedita alias illo suscipit nemo nostrum voluptatum ducimus, vero aliquam assumenda mollitia delectus, beatae fugit ipsa dignissimos tenetur, distinctio natus repellendus." },
		{ user: "radim", etguid: '123d', rating: 4, content: "neasi" },
		{ user: "radim", etguid: '321d', rating: 5, content: "neasi" },
		{ user: "radim", etguid: '322d', rating: 5, content: "neasi" },
	];

	layerDetailPanel.commentSection(comments);
}