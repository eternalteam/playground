/*-----------------------------------------------------------------------------*/
/* ListContainer CLASS */
class Panel {
    constructor(id) {
        this.destinationElement = document.querySelector('main');
        this.id = id;
        this.visible = false;
        //this.clearDestinationElement()
        this.elementInHtlm = document.getElementById(this.id);
        if (this.elementInHtlm) this.elementInHtlm.parentNode.removeChild(this.elementInHtlm);
        this.createContainer();
    }

    clearPanel() {
        if (this.container) {
            while (this.container.firstChild) {
                this.container.removeChild(this.container.firstChild);
            }

        }
    }

    createContainer() {
        this.container = document.createElement('section');

        this.container.classList.add('et-panel');
        this.container.classList.add('hidden');
        this.container.id = this.id;

        this.destinationElement.prepend(this.container);
    } 

    addChild(child) {
        this.container.appendChild(child);
    }
    
    hide() {
        this.container.classList.add('hidden');
        this.visible = false;
    }
    show() {
        this.container.classList.remove('hidden');
        this.visible = true;
    }
}
