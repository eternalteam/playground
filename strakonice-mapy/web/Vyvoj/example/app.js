let filter = {
    "filters": [
        {
            "filter-type": 6,
            "options": [
                { "option-type": 2, "option-title": "Pocity", "option-class-name": "", "option-color": "#FFFFFFFF" ,"option-value": "je_to_osklive,to_mate_rad,rad_nakupuji"},
                { "option-type": 3, "option-title": "Špatný pocit", "option-class-name": "", "option-color": "#51421FFF", "option-value": "je_to_osklive"},
                { "option-type": 3, "option-title": "Dobry pocit", "option-class-name": "", "option-color": "#F9C34EFF", "option-value": "to_mate_rad"},
                { "option-type": 3, "option-title": "Rád nakupuji", "option-class-name": "", "option-color": "#00B4CCFF", "option-value": "rad_nakupuji"},
            ]
        }, 
        {
            "filter-type": 8,
            "options": [
                { "option-type": 9, "option-title": "2017", "option-value": "mapbox://styles/eternal1/cjy6zoh4u0wc81cn07pguqjau"},
                { "option-type": 9, "option-title": "2018", "option-value": "mapbox://styles/eternal1/cjy8jc4km0yfj1cpgd8nuofo2"},
                { "option-type": 9, "option-title": "2019", "option-value": "mapbox://styles/eternal1/cjxzz9yq10u401cpeg7ehi7op"},
            ]
        }
    ]
};

function showPanel(obj)
{
    let data = document.getElementById('map').getAttribute('data-filters');
    //console.log(JSON.parse(data))
    let filterType = obj.type;
    let destination = document.getElementById('filters');

    let panel = new List(destination, filterType, data).build();
}

