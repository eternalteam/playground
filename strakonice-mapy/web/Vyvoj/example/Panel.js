

class List {
    constructor(destinationElement, reqFilterType, jsonCode) {
        this.destinationElement = destinationElement.firstElementChild;
        this.filters = JSON.parse(jsonCode).filters;
        this.reqFilterType = reqFilterType;
    }

    build() {
        this.ul = document.createElement('ul');
        this.ul.classList.add('et-panel-filter-list');

        this.addItems()

        while (this.destinationElement.firstChild) {
            this.destinationElement.removeChild(this.destinationElement.firstChild);
        }

        this.destinationElement.appendChild(this.ul);
    }

    addItems() {
        for (var i in this.filters) {
            if (this.filters[i]['filter-type'] == this.reqFilterType) {
                this.options = this.filters[i]['options'];
                for (var j in this.options) {
                    //this.item = itemTypes[this.options['option-type']];
                    this.item = new Item(JSON.stringify(this.options[j])).build();
                    this.ul.appendChild(this.item)
                }
            }
        }
    }
}

class Item {
    constructor(jsonCode) {
        this.options = JSON.parse(jsonCode);
        this.type = this.options["option-type"];
        this.title = this.options["option-title"];
        this.className = this.options["option-class-name"];
        this.color = this.options["option-color"];
        this.value = this.options["option-value"];
        //this.createSpecificItem()
    }

    build() {
        this.li = document.createElement('li');
        this.span = document.createElement('span');
        this.input = document.createElement('input');

        this.li.classList.add('et-panel-filter-list-item');
        this.input.type = 'checkbox';
        this.span.textContent = this.title;

        this.li.addEventListener('click', () => {
            if (this.input.checked == true) this.input.checked = false;
            else this.input.checked = true;
        })

        if (this.type === 3) {
            this.colorBox = document.createElement('div');
            this.colorBox.classList.add('et-filter-color-box');
            this.colorBox.style.background = this.color;
            this.li.appendChild(this.colorBox);
        } else if (this.type === 2) {
            this.li.classList.add('header');
            
            this.li.addEventListener('click', () => {
                this.x = this.li.parentElement.querySelectorAll('input[type="checkbox"]');
                this.x.forEach(e => e.checked = this.input.checked);
            })
        } else if (this.type === 9) {
            this.input.type = 'radio';
            this.input.name = 'date'
        }

        

        this.li.appendChild(this.span);
        this.li.appendChild(this.input);
        return this.li;
    }

    // createSpecificItem() {
    //     this.specificItem = itemTypes[this.type].build();
    // }
}












// class HeaderItem extends Item {
//     constructor(jsonCode) {
//         super(jsonCode)
//     }

//     build()
//     {
        
//         return "ree";
//     }
// }

// class ListItem extends Item {
//     constructor(jsonCode) {
        
//     }

//     build()
//     {
//         this.element = document.createElement('li');
//         this.container = document.createElement('label');
//         this.elementTitle = document.createElement('span');
//         this.input = document.createElement('input');

//         this.elementTitle.textContent = this.title;
//         this.input.type = 'checkbox';

//         if (this.type == 2) this.element.classList.add('et-panel-filter-header-item')
//         else if (this.type == 3) {
//             this.element.classList.add('et-panel-filter-list-item');
//             this.colorBox = document.createElement('div');
//             this.colorBox.style.background = this.color;
//             this.colorBox.classList.add('et-filter-color-box');
//             this.elementTitle.classList.add('et-filter-list-item-name');
//             this.container.appendChild(this.colorBox);
//         } else if (this.type == 9) {
//             this.element.classList.add('et-panel-filter-list-item');
//             this.input.type = 'radio';
//             this.input.name = "year";
//         };

//         this.container.appendChild(this.elementTitle);
//         this.container.appendChild(this.input);

//         this.element.appendChild(this.container);

//         return this.element;
//     }
// }

// class RadioItem extends Item {
//     constructor(jsonCode) {
        
//     }

//     build()
//     {
//         this.elementTitle.textContent = this.title;
//         this.input.type = 'checkbox';

//         if (this.type == 2) this.element.classList.add('et-panel-filter-header-item')
//         else if (this.type == 3) {
//             this.element.classList.add('et-panel-filter-list-item');
//             this.colorBox = document.createElement('div');
//             this.colorBox.style.background = this.color;
//             this.colorBox.classList.add('et-filter-color-box');
//             this.elementTitle.classList.add('et-filter-list-item-name');
//             this.container.appendChild(this.colorBox);
//         } else if (this.type == 9) {
//             this.element.classList.add('et-panel-filter-list-item');
//             this.input.type = 'radio';
//             this.input.name = "year";
//         };

//         this.container.appendChild(this.elementTitle);
//         this.container.appendChild(this.input);

//         this.element.appendChild(this.container);

//         return this.element;
//     }
// }