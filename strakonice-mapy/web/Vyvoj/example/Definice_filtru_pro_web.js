
   let filter = '{
        "filters":
        [
            {
                "filter-type": 8,
                "options": [
                    { "option-type": 9, "option-title": "2017", "option-value": "mapbox://styles/eternal1/cjy6zoh4u0wc81cn07pguqjau" },
                    { "option-type": 9, "option-title": "2018", "option-value": "mapbox://styles/eternal1/cjy8jc4km0yfj1cpgd8nuofo2" },
                    { "option-type": 9, "option-title": "2019", "option-value": "mapbox://styles/eternal1/cjxzz9yq10u401cpeg7ehi7op" },
                ]
            },
            {
                "filter-type": 6,
                "options": [
                    { "option-type": 2, "option-title": "Plochy s rozdílným způsobem využití", "option-class-name": "", "option-color": "", "option-value": "plocha_bydleni,plocha_bydleni_stav,bydleni_hromadne_v_bytovych_domech_blokova_zastavba,bydleni_hromadne_v_bytovych_domech_blokova_zastavba_stav,bydleni_individualni_v_rodinnych_domech_mestske,bydleni_individualni_v_rodinnych_domech_mestske_stav,bydleni_individualni_v_rodinnych_domech_vesnicke,bydleni_individualni_v_rodinnych_domech_vesnicke_stav,plocha_rekreace,blocha_rekreace_stav,plocha_rodinne_rekreace,plocha_rodinne_rekreace_stav,plocha_individualni_rekreace_zahradkarska_osada,plocha_individualni_rekreace_zahradkarska_osada_stav,plocha_obcanskeho_vybaveni,plocha_obcanskeho_vybaveni_stav,plocha_verejne_vybavenosti,plocha_verejne_vybavenosti_stav,plocha_pro_vzdelavani_a_vychovu,plocha_pro_vzdelavani_a_vychovu_stav,plocha_komercnich_zarizeni,plocha_komercnich_zarizeni_stav,plocha_pro_telovychovu_a_sport,plocha_pro_telovychovu_a_sport_stav,plocha_smisena_obytna,plocha_smisena_obytna_stav,plocha_smisena_obytna_komercni,plocha_smisena_obytna_komercni_stav,plocha_dopravni_infrastruktury,plocha_dopravni_infrastruktury_stav,plocha_pro_silnicni_dopravu,plocha_pro_silnicni_dopravu_stav,plocha_pro_drazni_dopravu,plocha_pro_drazni_dopravu_stav,plocha_pro_leteckou_dopravu,plocha_pro_leteckou_dopravu_stav,plocha_pro_vodni_hospodarstvi,plocha_pro_vodni_hospodarstvi_stav,plocha_vyroby_a_skladovani,plocha_vyroby_a_skladovani_stav,prumyslova_vyroba_a_sklady_lehky_prumysl,prumyslova_vyroba_a_sklady_lehky_prumysl_stav,plocha_pro_zemedelskou_a_lesnickou_vyrobu,plocha_pro_zemedelskou_a_lesnickou_vyrobu_stav,plocha_verejneho_prostranstvi_s_prevahou_zpevnenych_ploch,plocha_verejneho_prostranstvi_s_prevahou_zpevnenych_ploch_stav,plocha_verejnych_prostranstvi_s_prevahou_nezpevnenych_ploch,plocha_verejnych_prostranstvi_s_prevahou_nezpevnenych_ploch_stav,park_historicka_zahrada,park_historicka_zahrada_stav" },
                    { "option-type": 3, "option-title": "Plocha bydlení", "option-class-name": "mapcaption_plocha_bydleni", "option-color": "", "option-value": "plocha_bydleni,plocha_bydleni_stav" },
                    { "option-type": 3, "option-title": "Bydlení hromadné v bytových domech - bloková zástavba", "option-class-name": "mapcaption_bydleni_hromadne_v_bytovych_domech_blokova_zastavba", "option-color": "", "option-value": "bydleni_hromadne_v_bytovych_domech_blokova_zastavba,bydleni_hromadne_v_bytovych_domech_blokova_zastavba_stav" },
                    { "option-type": 3, "option-title": "Bydlení individuální v rodinných domech - městské", "option-class-name": "mapcaption_bydleni_individualni_v_rodinnych_domech_mestske", "option-color": "", "option-value": "bydleni_individualni_v_rodinnych_domech_mestske,bydleni_individualni_v_rodinnych_domech_mestske_stav" }
                ]
            }
        ];
    }'